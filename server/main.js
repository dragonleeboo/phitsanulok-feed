import {
    Meteor
} from 'meteor/meteor';
import moment from "moment-timezone";
import bodyParser from "body-parser";
import express from "express";
import cors from "cors";
import query_command from "./app/query";
const server = express();

server.use(
    cors(),
    bodyParser.json(),
    bodyParser.urlencoded({
        extended: true
    })
);
WebApp.connectHandlers.use(Meteor.bindEnvironment(server));

server.post("/server_time", (req, res) => {
    const {
        body
    } = req;
    res.send({
        server_time: new Date()
    });
});

server.post("/getalldevices", (req, res) => {
    const {
        body
    } = req;
    const query = 'SELECT * FROM devices;';
    query_command.queryfn(res, query);
});

server.post("/add_devices", (req, res) => {
    const {
        body
    } = req;
    const query = `INSERT INTO devices (id) VALUES  ('${body.id_devicce}');`;
    query_command.queryfn(res, query);
});

server.post("/remove_devices", (req, res) => {
    const {
        body
    } = req;
    const query = `DELETE FROM devices WHERE id = '${body.id_devicce}';`;
    query_command.queryfn(res, query);
});

server.post("/remove_devices", (req, res) => {
    const {
        body
    } = req;
    const query = `DELETE FROM devices WHERE id = '${body.id_devicce}';`;
    query_command.queryfn(res, query);
});


server.post("/find_history", (req, res) => {
    const {
        body
    } = req;
    const query = `SELECT
                    * 
                FROM
                    data_sensor 
                WHERE
                    data_sensor.TIMESTAMP >= '${body.start}' 
                    AND data_sensor.TIMESTAMP <= '${body.end}' 
                    AND data_sensor.device_id = '${body.device_id}'
                ORDER BY
                    data_sensor.TIMESTAMP ASC;`;
    // console.log(query);
    query_command.queryfn(res, query);
});

server.post("/get_last_update", (req, res) => {
    const {
        body
    } = req;
    const query = `SELECT * FROM data_sensor WHERE timestamp in (SELECT MAX(timestamp) from data_sensor GROUP BY device_id)`;
    // console.log(query);
    query_command.queryfn(res, query);
});

server.post("/add_data", async (req, res) => {
    const {
        body
    } = req;
    // console.log("message");
    console.log("receive >> " + JSON.stringify(body));
    var ts = moment().tz('Asia/Bangkok').format("YYYY-MM-DD HH:mm:ss")
    lastdata(body.device_id, body.pm10, body.pm25, ts);
    // console.log(body.device_id,body.pm10,body.pm25);
    // alertHight(body.device_id,body.pm10,body.pm25);
    const query = await query_command.queryre(`INSERT INTO data_sensor (device_id,timestamp,pm10,pm25,temp,humid) VALUES  ('${body.device_id}','${ts}','${body.pm10}','${body.pm25}','${body.temp}','${body.humid}');`);

    res.send(query)
});
// console.log(null);

async function lastdata(x, pm10, pm25, ts) {
    //results[0].linetoken
    var result = await query_command.queryre(`SELECT * FROM devices , alert
    WHERE devices.id = "${x}"`);
    if (result.length > 0) {
        // console.log(result[0])
        var result = result[0];
        const lineNotify = require('line-notify-nodejs')(result.linetoken);
        var arrCheck = parseInt(x.split("_")[1])
        console.log(result["area" + arrCheck + "check"], result["area" + arrCheck + "2check"]);
        var alerttext = "⚠️ พบค่าฝุ่นสูงกว่ากำหนด ⚠️"
        if ((result["area" + arrCheck + "check"] == "checked" || result["area" + arrCheck + "2check"] == "checked") && ((parseFloat(pm10) > parseFloat(result["area" + arrCheck + "value"])) || (parseFloat(pm25) > parseFloat(result["area" + arrCheck + "2value"])))) {
            alerttext += "\n📍 " + result.name
        }
        /////////////////////////// PM10 /////////////////////////////
        if (result["area" + arrCheck + "check"] == "checked") {
            if (parseFloat(pm10) > parseFloat(result["area" + arrCheck + "value"])) {
                if (result.datealrtpm10 == null) {
                    alerttext += "\n      PM10 : " + parseFloat(pm10).toFixed(1) + " μg/m3"
                    var setalert = await query_command.queryre(`UPDATE devices
                    SET datealrtpm10="${ts}"
                    WHERE id="${x}"`);
                } else {
                    var start = moment().tz('Asia/Bangkok')
                    var end = moment(result.datealrtpm10).tz('Asia/Bangkok')
                    const minutes = start.diff(end, "minutes");

                    if (minutes > 15) {
                        console.log(minutes);
                        alerttext += "\n      PM10 : " + parseFloat(pm10).toFixed(1) + " μg/m3"
                        var setalert = await query_command.queryre(`UPDATE devices
                        SET datealrtpm10="${ts}"
                        WHERE id="${x}"`);
                    }
                }
            } else {
                var setalert = await query_command.queryre(`UPDATE devices
                SET datealrtpm10 = NULL
                WHERE id="${x}"`);
            }
        }
        ////////////////////////////2.5///////////////////////////////
        if (result["area" + arrCheck + "2check"] == "checked") {
            if (parseFloat(pm25) > parseFloat(result["area" + arrCheck + "2value"])) {
                if (result.datealrtpm25 == null) {
                    alerttext += "\n      PM2.5 : " + parseFloat(pm25).toFixed(1) + " μg/m3"
                    var setalert = await query_command.queryre(`UPDATE devices
                    SET datealrtpm25="${ts}"
                    WHERE id="${x}"`);
                } else {
                    var start = moment().tz('Asia/Bangkok')
                    var end = moment(result.datealrtpm25).tz('Asia/Bangkok')
                    const minutes = start.diff(end, "minutes");
                    if (minutes > 15) {
                        alerttext += "\n      PM2.5 : " + parseFloat(pm25).toFixed(1) + " μg/m3"
                        var setalert = await query_command.queryre(`UPDATE devices
                        SET datealrtpm25="${ts}"
                        WHERE id="${x}"`);
                    }
                }
            } else {
                var setalert = await query_command.queryre(`UPDATE devices
                SET datealrtpm25 = NULL
                WHERE id="${x}"`);
            }
        }

        if (alerttext.length > 50) {
            console.log(alerttext);
            lineNotify.notify({
                message: alerttext,
            }).then(() => {
                console.log('send completed!');
            }).catch((err) => {
                console.log(err);
            });
            alerttext = ""
        }
    }
}

server.post("/get_setting", (req, res) => {
    const {
        body
    } = req;
    console.log("message");
    const query = `SELECT * FROM alert;`;
    query_command.queryfn(res, query);
});

server.post("/set_setting", (req, res) => {
    const {
        body
    } = req;
    // console.log(body);
    setTimeout(() => {
        initjob()
    }, 1000);

    const query = `UPDATE alert
    SET linetoken = '${body.linetoken}',
    area1check = '${body.area1check}',
    area2check = '${body.area2check}',
    area3check = '${body.area3check}',
    area4check = '${body.area4check}',
    area1value = ${body.area1value},
    area2value = ${body.area2value},
    area3value = ${body.area3value},
    area4value = ${body.area4value},
    area12check = '${body.area12check}',
    area22check = '${body.area22check}',
    area32check = '${body.area32check}',
    area42check = '${body.area42check}',
    area12value = ${body.area12value},
    area22value = ${body.area22value},
    area32value = ${body.area32value},
    area42value = ${body.area42value},
    avgpm10 = '${body.avgpm10}',
    avgpm25 = '${body.avgpm25}',
    notitime = '${body.notitime}'`;
    query_command.queryfn(res, query);
});
// errirLZSHRbagK0ik3fGpUjPGWRQKYpSJsGhLDFoA38  -- cake
// vpbeeFdUoIE18CnsTtJyPNxhmoaVBdYdQNrGpxPWuQS -- yea
// server.get("/test_noti", (req, res) => {
//     // 0EcwkXJJZHMaAFciPX0JoTJ9BnyzH3kyan2k1LvltYD
//     const lineNotify = require('line-notify-nodejs')('0EcwkXJJZHMaAFciPX0JoTJ9BnyzH3kyan2k1LvltYD');

// lineNotify.notify({
//   message: 'send test',
// }).then(() => {
//   console.log('send completed!');
// });

// });
initjob()

async function initjob() {
    console.log("==============");
    var CronJob = require('cron').CronJob;

    const results = await query_command.queryre('SELECT * FROM alert');
    var noti_h = results[0].notitime.split(':')[0]
    var noti_m = results[0].notitime.split(':')[1]
    console.log("LINE : " + results[0].linetoken);
    const lineNotify = require('line-notify-nodejs')(results[0].linetoken); //results[0].linetoken
    // console.log(noti_m + ' ' + noti_h + ' * * *');
    var avgjob = new CronJob(noti_m + ' ' + noti_h + ' * * *', async function () {
        const results_24hr = await query_command.queryre(`SELECT * FROM (SELECT * FROM ( SELECT device_id, AVG(data_sensor.pm10) AS avg_pm10, AVG(data_sensor.pm25) AS avg_pm25 FROM data_sensor WHERE TIMESTAMP > DATE_SUB(NOW(), INTERVAL 24 HOUR) GROUP BY device_id ) avg JOIN ( SELECT device_id as "d_id",pm10,pm25 FROM data_sensor WHERE TIMESTAMP IN ( SELECT max(TIMESTAMP) FROM data_sensor GROUP BY data_sensor.device_id ) ) last_data ON avg.device_id = last_data.d_id) datas,alert ORDER BY device_id`);
        var text = "\n📊 ค่าเฉลี่ย 24 ชั่วโมงย้อนหลัง\n"
        results_24hr.forEach(function (element, i) {
            text += avg24hr(element, "phs_001", "บริเวณโรงสีข้าว")
            text += avg24hr(element, "phs_002", "บริเวณถังดับเพลิง")
            text += avg24hr(element, "phs_003", "บริเวณหน้าโรงงาน")
            text += avg24hr(element, "phs_004", "บริเวณยานยนต์")
        });
        // console.log(text);
        if (results_24hr[0].avgpm10 == "checked" || results_24hr[0].avgpm25 == "checked") {
            lineNotify.notify({
                message: text,
            }).then(() => {
                console.log('send completed!');
            }).catch((err) => {
                console.log(err);
            });
            text = ""
        }
    }, null, true, 'Asia/Bangkok');
    avgjob.stop();
    avgjob.start();
    // var alertjob = new CronJob('*/15 * * * *', async function () {
    //     const results_alert = await query_command.queryre(`SELECT * FROM (SELECT * FROM ( SELECT device_id, AVG(data_sensor.pm10) AS avg_pm10, AVG(data_sensor.pm25) AS avg_pm25 FROM data_sensor WHERE TIMESTAMP > DATE_SUB(NOW(), INTERVAL 24 HOUR) GROUP BY device_id ) avg JOIN ( SELECT device_id as "d_id",pm10,pm25 FROM data_sensor WHERE TIMESTAMP IN ( SELECT max(TIMESTAMP) FROM data_sensor GROUP BY data_sensor.device_id ) ) last_data ON avg.device_id = last_data.d_id) datas,alert ORDER BY device_id`);
    //     var alerttext = "⚠️ พบค่าฝุ่นสูงกว่ากำหนด ⚠️"
    //     setTimeout(() => {
    //         for (let index = 0; index < results_alert.length; index++) {
    //             const element = results_alert[index];
    //             alerttext += alertcheck(element, "phs_001", "บริเวณโรงสีข้าว", "1", "12")
    //             alerttext += alertcheck(element, "phs_002", "บริเวณถังดับเพลิง", "2", "22")
    //             alerttext += alertcheck(element, "phs_003", "บริเวณหน้าโรงงาน", "3", "32")
    //             alerttext += alertcheck(element, "phs_004", "บริเวณยานยนต์", "4", "42")
    //         }
    //         if (alerttext.length > 30) {
    //             console.log(alerttext);
    //             lineNotify.notify({
    //                 message: alerttext,
    //             }).then(() => {
    //                 console.log('send completed!');
    //             }).catch((err) => {
    //                 console.log(err);
    //             });
    //             alerttext = ""
    //         }
    //     }, 1000);
    // }, null, true, 'Asia/Bangkok');
    // alertjob.stop();
    // alertjob.start();
    // lineNotify.notify({
    //     message: "ตั้งค่าการแจ้งเตือนใหม่ เรียบร้อย",
    // }).then(() => {
    //     console.log('send completed!');
    // }).catch((err) => {
    //     console.log(err);
    // });
}



function alertcheck(element, x, y, a, b) {
    var alerttext = ""
    if (element.device_id == x) {
        if ((element["area" + a + "check"] == "checked" || element["area" + b + "check"] == "checked") && ((parseFloat(element.pm10) > parseFloat(element["area" + a + "value"])) || (parseFloat(element.pm25) > parseFloat(element["area" + b + "value"])))) {
            alerttext += "\n📍 " + y
        }

        if (element["area" + a + "check"] == "checked") {

            if (parseFloat(element.pm10) > parseFloat(element["area" + a + "value"])) {
                alerttext += "\n      PM10 : " + parseFloat(element.pm10).toFixed(1) + " μg/m3"
            }
        }

        if (element["area" + b + "check"] == "checked") {
            console.log(parseFloat(element.pm10), parseFloat(element["area" + b + "value"]));
            if (parseFloat(element.pm25) > parseFloat(element["area" + b + "value"])) {
                alerttext += "\n      PM2.5 : " + parseFloat(element.pm25).toFixed(1) + " μg/m3"
            }
        }

    }
    return alerttext
}

function avg24hr(element, x, y) {

    var texts = ""
    if (element.device_id == x && (element.avgpm10 == "checked" || element.avgpm25 == "checked")) {
        texts += "\n📍 " + y
    }
    if (element.device_id == x && element.avgpm10 == "checked") {
        texts += "\n      PM10  : " + parseFloat(element.avg_pm10 ? element.avg_pm10 : 0).toFixed(2) + " μg/m3"
    }
    if (element.device_id == x && element.avgpm25 == "checked") {
        texts += "\n      PM2.5  : " + parseFloat(element.avg_pm25 ? element.avg_pm25 : 0).toFixed(2) + " μg/m3"
    }
    // console.log(texts);
    return texts
}

checkoffline();
var alert_offline = [1,1,1,1];
var alert_offline_4_hr = [1,1,1,1];
async function checkoffline() {
    setInterval(async () => {
    const settings = await query_command.queryre('SELECT * FROM alert');
    const lineNotify = require('line-notify-nodejs')(settings[0].linetoken);
    const query = await query_command.queryre(`SELECT * FROM data_sensor
            JOIN devices ON data_sensor.device_id = devices.id
            WHERE data_sensor.timestamp IN 
            (SELECT max(data_sensor.timestamp) FROM data_sensor GROUP BY data_sensor.device_id) 
             `);
    for (let index = 0; index < query.length; index++) {
        const element = query[index];
        // console.log(index);
        device_id = "phs_00" + (index + 1)
        var dt_now = moment().tz('Asia/Bangkok');
        var dt_last = moment(element.timestamp).tz('Asia/Bangkok');
        diff_min = dt_now.diff(dt_last, 'minutes')
        diff_hr = dt_now.diff(dt_last, 'hours')
        if (element.device_id == device_id) {
            
            
            if (diff_min > 10) {
                if(alert_offline[index] == 0){
                    alert_offline[index] = 1
                    var ts = moment(element.timestamp).tz('Asia/Bangkok').format("DD/MM/YYYY HH:mm:ss น.")
                    var text = "\n⛔️ แจ้งเตือนระบบหยุดส่งข้อมูล ⛔️\n"
                    text += "📍 " + element.name + "\n"
                    text += "⏱ เมื่อ " + ts
                    lineNotify.notify({
                        message: text,
                    }).then(() => {
                        // console.log('send completed!');
                    }).catch((err) => {
                        console.log(err);
                    });
                }
            }else{
                alert_offline[index] = 0
            }

            var four_hr = diff_hr%4;
            console.log(element.name,diff_hr,diff_min);
            
            if(diff_hr != 0 && four_hr == 0){
                if(alert_offline_4_hr[index] == 0){
                    alert_offline_4_hr[index] = 1
                    var ts = moment(element.timestamp).tz('Asia/Bangkok').format("DD/MM/YYYY HH:mm:ss น.")
                    var text = "\n⛔️ แจ้งเตือนระบบหยุดส่งข้อมูล ⛔️\n"
                    text += "📍 " + element.name + "\n"
                    text += "⏱ เมื่อ " + ts
                    lineNotify.notify({
                        message: text,
                    }).then(() => {
                        // console.log('send completed!');
                    }).catch((err) => {
                        console.log(err);
                    });
                }
                
            }else{
                alert_offline_4_hr[index] = 0
            }

        }
        // console.log(element.device_id,element.name);

    }
    console.log("====");
    console.log(alert_offline);
    console.log(alert_offline_4_hr);

    }, 600000); // 600000
}
