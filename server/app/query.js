import mysql from 'mysql'

const mysqlconfig = {
    host: "178.128.98.92",
    user: "airhub",
    password: "Airhub@thailand20202020",
    database: "airhub",
    timezone: 'ict',
    insecureAuth: true
}


const queryfn = (res, query) => {
    // console.log(query);
    return new Promise(function(resolve, reject) {
        const con = mysql.createConnection(mysqlconfig)
        con.connect(function(err) {
            con.query(query, function(err, result, fields) {
                if (err) {
                    console.log(err)
                    res.send({
                        "errorCode": err.code
                    })
                } else {
                    resolve(result)
                    res.send(result)
                }
                con.end();
            })
        })
    })
}

const queryre = (options, value = []) => {
    return new Promise((resolve, reject) => {
        const connection = mysql.createConnection(mysqlconfig)
      connection.query(options, value, function (error, results, fields) {
        if (error) {
          resolve(error);
          // reject(error);
        } else {
          resolve(JSON.parse(JSON.stringify(results)));
        }
        connection.end();
      });
    });
  };

// export default queryfn
module.exports = { 
    queryre,
    queryfn
  };