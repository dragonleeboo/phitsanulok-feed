Router.route('/', function() {
    Router.go('dashboard');
});
Router.route('/dashboard', function() {
    Session.set('dashboard',true);
    Session.set('history',null);
    Session.set('alert',null);
    Session.set('contents',null);
    this.render('dashboard');
});
Router.route('/history', function() {
    Session.set('dashboard',null);
    Session.set('history',true);
    Session.set('alert',null);
    Session.set('contents',null);
    this.render('history');
});
Router.route('/alert', function() {
    Session.set('dashboard',null);
    Session.set('history',null);
    Session.set('alert',true);
    Session.set('contents',null);
    this.render('alert');
});
Router.route('/contents', function() {
    Session.set('dashboard',null);
    Session.set('history',null);
    Session.set('alert',null);
    Session.set('contents',true);
    this.render('contents');
});


Router.route('/ddash', function() {
    this.render('ddash');
});

Router.route('/devices', function() {
    this.render('devices');
});
