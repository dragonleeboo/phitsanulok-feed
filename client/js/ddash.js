const axios = require('axios')
Template.ddash.onRendered(function() {
    $(document).ready(function() {
        $(".GaugeMeter").gaugeMeter();
        $('.tooltipped').tooltip();
        axios.post(`${Meteor.settings.public.api_host}/get_last_update`, {

        }).then((response) => {
            console.log(response.data);
            updatesensor(response.data)
        });
        setInterval(() => {
            axios.post(`${Meteor.settings.public.api_host}/get_last_update`, {

            }).then((response) => {
                updatesensor(response.data)
            });
        }, 5000);
    });
})



Template.ddash.helpers({
    offp1: function() {
        return Session.get('offp1')
    },
    offp2: function() {
        return Session.get('offp2')
    },
    offp3: function() {
        return Session.get('offp3')
    },
    offp4: function() {
        return Session.get('offp4')
    },
    p1ago() {
        return Session.get('p1ago')
    },
    p2ago() {
        return Session.get('p2ago')
    },
    p3ago() {
        return Session.get('p3ago')
    },
    p4ago() {
        return Session.get('p4ago')
    },
    tm1() {
        return Session.get('tm1')
    },
    tm2() {
        return Session.get('tm2')
    },
    tm3() {
        return Session.get('tm3')
    },
    tm4() {
        return Session.get('tm4')
    }
});

Template.ddash.events({
    'click #foo': function(event, template) {

    }
});

function updatesensor(x) {
    for (let index = 0; index < x.length; index++) {
        const element = x[index];
        var color10 = ""
        if (element.pm10 > 181) {
            color10 = 'red'
        } else if (element.pm10 >= 120) {
            color10 = 'ORANGE'
        } else if (element.pm10 >= 80) {
            color10 = '#FCE86A'
        } else if (element.pm10 >= 50) {
            color10 = '#00FF00'
        } else if (element.pm10 >= 0) {
            color10 = 'Cyan'
        }

        var color25 = ""
        if (element.pm25 > 91) {
            color25 = 'red'
        } else if (element.pm25 >= 50) {
            color25 = 'ORANGE'
        } else if (element.pm25 >= 37) {
            color25 = '#FCE86A'
        } else if (element.pm25 >= 25) {
            color25 = '#00FF00'
        } else if (element.pm25 >= 0) {
            color25 = 'Cyan'
        }

        var pm10_real = element.pm10;
        var pm10 = parseInt((pm10_real / 180) * 100)

        var pm25_real = element.pm25;
        var pm25 = parseInt((pm25_real / 90) * 100)

        var start = moment(element.timestamp).tz('Asia/Bangkok')
        var end = moment(Session.get('s_time')).tz('Asia/Bangkok')

        const minutes = start.diff(end, "minutes");
        // console.log(minutes);
        var ago = moment.duration(moment(Session.get('s_time')).tz('Asia/Bangkok').diff(moment(element.timestamp).tz('Asia/Bangkok')))
        var hours = parseInt(ago.asHours());
        var minute = parseInt(ago.asMinutes()) - hours * 60;
        t = moment(element.timestamp).tz('Asia/Bangkok').tz('Asia/Bangkok').format('DD/MM/YYYY HH:mm:ss');
        ago = t + " (เมื่อ " + hours + ' ชม. ' + minute + ' ' + "น. ที่แล้ว)"
        if (element.device_id == "phs_001") {
            // console.log("phs_001", moment.utc(element.timestamp).tz('Asia/Bangkok').tz('Asia/Bangkok').format('DD/MM/YYYY HH:mm:ss'), element.timestamp);
            $("#GaugeMeter_1").gaugeMeter({ percent: pm10, text: parseFloat(element.pm10).toFixed(1), color: color10 });
            $("#GaugeMeter_2").gaugeMeter({ percent: pm25, text: parseFloat(element.pm25).toFixed(1), color: color25 });
            
            Session.set('p1ago', ago)
            if (minutes < -10) {
                Session.set('offp1', true)
            } else {
                Session.set('offp1', false)
            }
            Session.set('tm1',{temp:element.temp,humid:element.humid})
        } else if (element.device_id == "phs_002") {
            $("#GaugeMeter_3").gaugeMeter({ percent: pm10, text: parseFloat(element.pm10).toFixed(1), color: color10 });
            $("#GaugeMeter_4").gaugeMeter({ percent: pm25, text: parseFloat(element.pm25).toFixed(1), color: color25 });
            Session.set('p2ago', ago)
            if (minutes < -10) {
                Session.set('offp2', true)
            } else {
                Session.set('offp2', false)
            }
            Session.set('tm2',{temp:element.temp,humid:element.humid})
        } else if (element.device_id == "phs_003") {
            // console.log("phs_003");
            $("#GaugeMeter_5").gaugeMeter({ percent: pm10, text: parseFloat(element.pm10).toFixed(1), color: color10 });
            $("#GaugeMeter_6").gaugeMeter({ percent: pm25, text: parseFloat(element.pm25).toFixed(1), color: color25 });
            Session.set('p3ago', ago)
            if (minutes < -10) {
                Session.set('offp3', true)
            } else {
                Session.set('offp3', false)
            }
            Session.set('tm3',{temp:element.temp,humid:element.humid})
        } else if (element.device_id == "phs_004") {
            // console.log(element.timestamp);
            // console.log(minutes);
            $("#GaugeMeter_7").gaugeMeter({ percent: pm10, text: parseFloat(element.pm10).toFixed(1), color: color10 });
            $("#GaugeMeter_8").gaugeMeter({ percent: pm25, text: parseFloat(element.pm25).toFixed(1), color: color25 });
            Session.set('p4ago', ago)
            if (minutes < -10) {
                Session.set('offp4', true)
            } else {
                Session.set('offp4', false)
            }
            Session.set('tm4',{temp:element.temp,humid:element.humid})
        }
    }
}