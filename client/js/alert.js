const axios = require('axios')
Template.alert.onRendered(function () {
    $('.timepicker').timepicker({
        vibrate: true,
        twelveHour: false
    });
})

Template.alert.onCreated(() => {
    $(document).ready(function () {
        M.AutoInit();
        $('.sidenav').sidenav();
    });
    axios.post(`${Meteor.settings.public.api_host}/get_setting`, {

    }).then((response) => {

        console.log(response.data);
        Session.set("setting",response.data[0])
    }).catch((err) => {

        console.log(err);
    })

})

Template.alert.helpers({ 
    setting: function() { 
        return Session.get('setting')
    }, 
    rendered: function() { 
         
    }, 
    destroyed: function() { 
         
    }, 
}); 

Template.alert.events({ 
    'click #save': function(event, template) { 
        axios.post(`${Meteor.settings.public.api_host}/set_setting`, {
            linetoken:$("#token").val(),
            area1check:$("#area1check").prop('checked') == true ? "checked" : "",
            area2check:$("#area2check").prop('checked') == true ? "checked" : "",
            area3check:$("#area3check").prop('checked') == true ? "checked" : "",
            area4check:$("#area4check").prop('checked') == true ? "checked" : "",
            area12check:$("#area12check").prop('checked') == true ? "checked" : "",
            area22check:$("#area22check").prop('checked') == true ? "checked" : "",
            area32check:$("#area32check").prop('checked') == true ? "checked" : "",
            area42check:$("#area42check").prop('checked') == true ? "checked" : "",
            area1value:$("#place1").val(),
            area2value:$("#place2").val(),
            area3value:$("#place3").val(),
            area4value:$("#place4").val(),
            area12value:$("#place12").val(),
            area22value:$("#place22").val(),
            area32value:$("#place32").val(),
            area42value:$("#place42").val(),
            avgpm10:$("#avgpm10").prop('checked') == true ? "checked" : "",
            avgpm25:$("#avgpm25").prop('checked') == true ? "checked" : "",
            notitime:$("#pickertime").val(),
        }).then((response) => {
            alert("ตั้งค่าเรียบร้อย")
        }).catch((err) => {
    
            console.log(err);
        })
    } 
}); 
