

Template.navbar.onCreated(() => {
    $(document).ready(function () {
        M.AutoInit();
        
        setTimeout(() => {
            $('.sidenav').sidenav();
        }, 500);
    });
})
Template.navbar.helpers({
    dashboard: function () {
        return Session.get('dashboard');
    },
    history: function () {
        return Session.get('history');
    },
    alert: function () {
        return Session.get('alert');
    },
    contents: function () {
        return Session.get('contents');
    },
});

Template.navbar.events({
    'click li': function (event, template) {
        $('.sidenav').sidenav()
    },
    'click #dashboard'() {
        Session.set('dashboard',true);
        Session.set('history',null);
        Session.set('alert',null);
        Session.set('contents',null);
    },
    'click #history'() {
        Session.set('dashboard',null);
        Session.set('history',true);
        Session.set('alert',null);
        Session.set('contents',null);
    },
    'click #alert'() {
        Session.set('dashboard',null);
        Session.set('history',null);
        Session.set('alert',true);
        Session.set('contents',null);
    },
    'click #contents'() {
        Session.set('dashboard',null);
        Session.set('history',null);
        Session.set('alert',null);
        Session.set('contents',true);
    },
});