import moment from 'moment-timezone';
const axios = require('axios')
Template.history.onRendered(function () {
    // initchart()
    Session.set('start1', moment().subtract(1, 'days').tz('Asia/Bangkok').tz('Asia/Bangkok').format('DD/MM/YYYY HH:mm'))
    Session.set('start2', moment().tz('Asia/Bangkok').tz('Asia/Bangkok').format('DD/MM/YYYY HH:mm'))
    Session.set('start3', moment().subtract(1, 'days').tz('Asia/Bangkok').tz('Asia/Bangkok').format('DD/MM/YYYY HH:mm'))
    Session.set('start4', moment().tz('Asia/Bangkok').tz('Asia/Bangkok').format('DD/MM/YYYY HH:mm'))
    Session.set('start5', moment().subtract(1, 'days').tz('Asia/Bangkok').tz('Asia/Bangkok').format('DD/MM/YYYY HH:mm'))
    Session.set('start6', moment().tz('Asia/Bangkok').tz('Asia/Bangkok').format('DD/MM/YYYY HH:mm'))
    Session.set('start7', moment().subtract(1, 'days').tz('Asia/Bangkok').tz('Asia/Bangkok').format('DD/MM/YYYY HH:mm'))
    Session.set('start8', moment().tz('Asia/Bangkok').tz('Asia/Bangkok').format('DD/MM/YYYY HH:mm'))
    initchart(1, {
        title: 'บริเวณโรงสีข้าว',
        labels: [],
        pm10: [],
        pm25: [],
    })
    initchart(2, {
        title: 'บริเวณดังดับเพลิง',
        labels: [],
        pm10: [],
        pm25: [],
    })
    initchart(3, {
        title: 'บริเวณหน้าโรงงาน',
        labels: [],
        pm10: [],
        pm25: [],
    })
    initchart(4, {
        title: 'บริเวณยานยนต์',
        labels: [],
        pm10: [],
        pm25: [],
    })
    $('.datepicker').datepicker({
        // "autoClose":true,
        "format": "dd/mm/yyyy",
        "setDefaultDate": true,
        "defaultDate": new Date()
    });
    $('.timepicker').timepicker({
        vibrate: true,
        twelveHour: false,
        // autoClose:true,
        defaultTime: moment().tz('Asia/Bangkok').format('HH:mm'),
        onCloseEnd: function () {
            console.log(Session.get('clickselect'));
            if (Session.get('clickselect') == "start1") {
                Session.set('start1', $('#pickerdate_start1').val() + ' ' + $('#pickertime_start1').val())
            } else if (Session.get('clickselect') == "start2") {
                Session.set('start2', $('#pickerdate_start2').val() + ' ' + $('#pickertime_start2').val())
            } else if (Session.get('clickselect') == "start3") {
                Session.set('start3', $('#pickerdate_start3').val() + ' ' + $('#pickertime_start3').val())
            } else if (Session.get('clickselect') == "start4") {
                Session.set('start4', $('#pickerdate_start4').val() + ' ' + $('#pickertime_start4').val())
            } else if (Session.get('clickselect') == "start5") {
                Session.set('start5', $('#pickerdate_start5').val() + ' ' + $('#pickertime_start5').val())
            } else if (Session.get('clickselect') == "start6") {
                Session.set('start6', $('#pickerdate_start6').val() + ' ' + $('#pickertime_start6').val())
            } else if (Session.get('clickselect') == "start7") {
                Session.set('start7', $('#pickerdate_start7').val() + ' ' + $('#pickertime_start7').val())
            } else if (Session.get('clickselect') == "start8") {
                Session.set('start8', $('#pickerdate_start8').val() + ' ' + $('#pickertime_start8').val())
            }

        }
    });
})

Template.history.helpers({
    start1: function () {
        return Session.get('start1')
    },
    start2: function () {
        return Session.get('start2')
    },
    start3: function () {
        return Session.get('start3')
    },
    start4: function () {
        return Session.get('start4')
    },
    start5: function () {
        return Session.get('start5')
    },
    start6: function () {
        return Session.get('start6')
    },
    start7: function () {
        return Session.get('start7')
    },
    start8: function () {
        return Session.get('start8')
    },
});

Template.history.events({
    'click #start1': function (event, template) {
        Session.set('clickselect', 'start1')
        $('#pickertime_start1').click()
        $('#pickerdate_start1').click()
    },
    'click #start2': function (event, template) {
        Session.set('clickselect', 'start2')
        $('#pickertime_start2').click()
        $('#pickerdate_start2').click()
    },
    'click #start3': function (event, template) {
        Session.set('clickselect', 'start3')
        $('#pickertime_start3').click()
        $('#pickerdate_start3').click()
    },
    'click #start4': function (event, template) {
        console.log('222');
        Session.set('clickselect', 'start4')
        $('#pickertime_start4').click()
        $('#pickerdate_start4').click()
    },
    'click #start5': function (event, template) {
        console.log('222');

        Session.set('clickselect', 'start5')
        $('#pickertime_start5').click()
        $('#pickerdate_start5').click()
    },
    'click #start6': function (event, template) {
        Session.set('clickselect', 'start6')
        $('#pickertime_start6').click()
        $('#pickerdate_start6').click()
    },
    'click #start7': function (event, template) {
        Session.set('clickselect', 'start7')
        $('#pickertime_start7').click()
        $('#pickerdate_start7').click()
    },
    'click #start8': function (event, template) {
        Session.set('clickselect', 'start8')
        $('#pickertime_start8').click()
        $('#pickerdate_start8').click()
    },
    'click #search1'() {
        var labels = [],
            pm10 = [],
            pm25 = [];
            temp = [];
            humidi = [];
        axios.post(`${Meteor.settings.public.api_host}/find_history`, {
            start: formatdate($('#start1').val()),
            end: formatdate($('#start2').val()),
            device_id: "phs_001"
        }).then((response) => {
            var data = response.data;
            for (let index = 0; index < data.length; index++) {
                const element = data[index];
                labels.push(moment(element.timestamp).tz('Asia/Bangkok').format('DD/MM/YYYY HH:mm:ss'))
                pm10.push(element.pm10)
                pm25.push(element.pm25)
                temp.push(element.temp)
                humidi.push(element.humid)
            }
            var content = {
                title: 'บริเวณโรงสีข้าว',
                labels,
                pm10,
                pm25,
                temp,
                humidi
            }
            initchart(1, content)
        });

    },
    'click #search2'() {
        var labels = [],
        pm10 = [],
        pm25 = [];
        temp = [];
        humidi = [];
        axios.post(`${Meteor.settings.public.api_host}/find_history`, {
            start: formatdate($('#start3').val()),
            end: formatdate($('#start4').val()),
            device_id: "phs_002"
        }).then((response) => {
            var data = response.data;
            for (let index = 0; index < data.length; index++) {
                const element = data[index];
                labels.push(moment(element.timestamp).tz('Asia/Bangkok').format('DD/MM/YYYY HH:mm:ss'))
                pm10.push(element.pm10)
                pm25.push(element.pm25)
                temp.push(element.temp)
                humidi.push(element.humid)
            }
            var content = {
                title: 'บริเวณดังดับเพลิง',
                labels,
                pm10,
                pm25,
                temp,
                humidi
            }
            initchart(2, content)
        });
    },
    'click #search3'() {
        var labels = [],
        pm10 = [],
        pm25 = [];
        temp = [];
        humidi = [];
        axios.post(`${Meteor.settings.public.api_host}/find_history`, {
            start: formatdate($('#start5').val()),
            end: formatdate($('#start6').val()),
            device_id: "phs_003"
        }).then((response) => {
            var data = response.data;
            for (let index = 0; index < data.length; index++) {
                const element = data[index];
                labels.push(moment(element.timestamp).tz('Asia/Bangkok').format('DD/MM/YYYY HH:mm:ss'))
                pm10.push(element.pm10)
                pm25.push(element.pm25)
                temp.push(element.temp)
                humidi.push(element.humid)
            }
            var content = {
                title: 'บริเวณหน้าโรงงาน',
                labels,
                pm10,
                pm25,
                temp,
                humidi
            }
            initchart(3, content)
        });
    },
    'click #search4'() {
        var labels = [],
        pm10 = [],
        pm25 = [];
        temp = [];
        humidi = [];
        axios.post(`${Meteor.settings.public.api_host}/find_history`, {
            start: formatdate($('#start7').val()),
            end: formatdate($('#start8').val()),
            device_id: "phs_004"
        }).then((response) => {
            var data = response.data;
            for (let index = 0; index < data.length; index++) {
                const element = data[index];
                labels.push(moment(element.timestamp).tz('Asia/Bangkok').format('DD/MM/YYYY HH:mm:ss'))
                pm10.push(element.pm10)
                pm25.push(element.pm25)
                temp.push(element.temp)
                humidi.push(element.humid)
            }
            var content = {
                title: 'บริเวณยานยนต์',
                labels,
                pm10,
                pm25,
                temp,
                humidi
            }
            initchart(4, content)
        });
    }
});

function formatdate(x) {
    var a = x.split(" ");
    var b = a[0].split("/");
    var c = b[2] + '/' + b[1] + '/' + b[0] + " " + a[1];
    return c
}

function initchart(x, content) {
    $('#chart' + x).remove();
    $('.chart-container' + x).append('<canvas id="chart' + x + '"><canvas>');
    var config = {
        type: 'line',
        data: {
            labels: content.labels,
            datasets: [{
                label: 'PM10',
                backgroundColor: window.chartColors.red,
                borderColor: window.chartColors.red,
                data: content.pm10,
                yAxisID: "y-axis-1",
                fill: false,
            }, {
                label: 'PM2.5',
                fill: false,
                backgroundColor: window.chartColors.blue,
                borderColor: window.chartColors.blue,
                data: content.pm25,
                yAxisID: "y-axis-2",
            }, {
                label: 'Temperature',
                fill: false,
                backgroundColor: window.chartColors.green,
                borderColor: window.chartColors.green,
                data: content.temp,
                yAxisID: "y-axis-3",
            }, {
                label: 'Humidity',
                fill: false,
                backgroundColor: window.chartColors.yellow,
                borderColor: window.chartColors.yellow,
                data: content.humidi,
                yAxisID: "y-axis-4",
            }]
        },
        options: {
            maintainAspectRatio: false,
            title: {
                display: true,
                text: content.title,
                fontSize: 16,
                fontFamily: 'Mitr'
            },
            tooltips: {
                mode: 'index',
                intersect: false,
                fontSize: 16,
                fontFamily: 'Mitr',
                // mode: "label",
                // titleMarginBottom: 8,
                // bodySpacing: 8,
                // xPadding: 15,
                // yPadding: 10,
                // cornerRadius: 2,
                // bodyFontStyle: "bold",
                // backgroundColor: "#fcfcfc",
                // titleFontColor: "#3a3938",
                // bodyFontColor: "#3a3938",
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            elements: {
                point:{
                    radius: 0
                }
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'DATE',
                        fontSize: 16,
                        fontFamily: 'Mitr'
                    }
                }],
                yAxes: [{
                    position: "left",
                    gridLines: {
                        color: "rgba(0, 0, 0, 0)",
                    },
                    ticks: {
                        beginAtZero: true,stepSize:10,
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'PM 10'
                    },
                    id: "y-axis-1"
                }, {
                    position: "left",
                    gridLines: {
                        color: "rgba(0, 0, 0, 0)",
                    },
                    ticks: {
                        beginAtZero: true,stepSize:10,
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'PM 2.5'
                    },
                    id: "y-axis-2"
                }, {
                    position: "right",
                    gridLines: {
                        color: "rgba(0, 0, 0, 0)",
                    },
                    ticks: {
                        beginAtZero: true,stepSize:10,
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Temperature'
                    },
                    id: "y-axis-3"
                }, {
                    position: "right",
                    gridLines: {
                        color: "rgba(0, 0, 0, 0)",
                    },
                    ticks: {
                        beginAtZero: true,stepSize:10,
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Humidity'
                    },
                    id: "y-axis-4"
                }]
            }
        }
    };

    window.myLine = new Chart('chart' + x, config);
}