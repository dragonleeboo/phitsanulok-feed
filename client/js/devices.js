const axios = require('axios')
Template.devices.onCreated(() => {
    axios.post(`${Meteor.settings.public.api_host}/getalldevices`, {

    }).then((response) => {
        // console.log(response.data);
        Session.set('devices', response.data)
    });
    $(document).ready(function () {
        Streamy.on("udp", function (d, s) {
            // console.log(d, s);
            $('#textarea1').append(" > " + moment().tz('Asia/Bangkok').tz('Asia/Bangkok').format('DD/MM/YYYY HH:mm:ss') + " : " + d.data + "\n");
            var textarea = document.getElementById('textarea1');
        textarea.scrollTop = textarea.scrollHeight;
        });
    });
});
Template.devices.helpers({
    devices: function () {
        return Session.get('devices')
    },
    rendered: function () {

    },
    destroyed: function () {

    },
});


Template.devices.events({
    'click #add': function (event, template) {
        if ($('#id_devicce').val()) {
            axios.post(`${Meteor.settings.public.api_host}/add_devices`, {
                "id_devicce": $('#id_devicce').val()
            }).then((responses) => {
                console.log(responses.data);
                if (responses.data.errorCode) {
                    alert("ID ซ้ำ")
                }
                axios.post(`${Meteor.settings.public.api_host}/getalldevices`, {

                }).then((response) => {
                    // console.log(response.data);
                    $('#id_devicce').val("")
                    Session.set('devices', response.data)
                });
            });

        }
    },
    'click #delete': function (event, template) {
        var txt;
        var r = confirm("ต้องการลบ " + this.id);
        if (r == true) {
            axios.post(`${Meteor.settings.public.api_host}/remove_devices`, {
                "id_devicce": this.id
            }).then((responses) => {
                axios.post(`${Meteor.settings.public.api_host}/getalldevices`, {

                }).then((response) => {
                    Session.set('devices', response.data)
                });
            });
        } else {}




    }

});