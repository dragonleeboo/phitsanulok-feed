const axios = require('axios')
Template.dashboard.onCreated(() => {
    $(document).ready(function() {
        M.AutoInit();
        setTimeout(() => {
            $('.sidenav').sidenav();
        }, 500);

        axios.post(`${Meteor.settings.public.api_host}/server_time`, {

        }).then((response) => {

            // console.log(response.data);
            var t = moment(response.data.server_time).tz('Asia/Bangkok').tz('Asia/Bangkok').format('DD/MM/YYYY HH:mm:ss');
            Session.set('time', t)
            var s = 0
            setInterval(() => {
                s += 1
                    // console.log(s,response.data.server_time);
                var t = moment(response.data.server_time).add(s, 'seconds').tz('Asia/Bangkok').tz('Asia/Bangkok').format('DD/MM/YYYY HH:mm:ss');
                Session.set('s_time', response.data.server_time)
                Session.set('time', t)
            }, 1000);
        });



    });
})
Template.dashboard.helpers({
    time: function() {

        return Session.get('time')
    },
    staion1_1: function() {
        return Session.get('staion1_1')
    },
    staion1_2: function() {
        return Session.get('staion1_2')
    },
    staion2_1: function() {
        return Session.get('staion2_1')
    },
    staion2_2: function() {
        return Session.get('staion2_2')
    },
    staion3_1: function() {
        return Session.get('staion3_1')
    },
    staion3_2: function() {
        return Session.get('staion3_2')
    },
    staion4_1: function() {
        return Session.get('staion4_1')
    },
    staion4_2: function() {
        return Session.get('staion4_2')
    },
});

Template.dashboard.events({
    'click #foo': function(event, template) {

    }
});